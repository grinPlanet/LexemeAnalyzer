package lexemeanalizer.model;

public class ObjLexicalAnalizer {

    private Token token;
    private String lexeme;
    private int start;
    private int length;
    private int position;

    public ObjLexicalAnalizer(Token token, String lexeme, int start, int length, int position) {
        this.token = token;
        this.lexeme = lexeme;
        this.start = start;
        this.length = length;
        this.position = position;
    }

    public ObjLexicalAnalizer() {}

    public Token getToken() {
        return token;
    }

    public void setToken(Token token) {
        this.token = token;
    }

    public String getLexeme() {
        return lexeme;
    }

    public void setLexeme(String lexeme) {
        this.lexeme = lexeme;
    }

    public int getStart() {
        return start;
    }

    public void setStart(int start) {
        this.start = start;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    @Override
    public String toString() {
        return "ObjLexicalAnalizer{" + "token=" + token + ", lexeme=" + lexeme + ", start=" + start + ", length=" + length + ", position=" + position + '}';
    }

    
    
}

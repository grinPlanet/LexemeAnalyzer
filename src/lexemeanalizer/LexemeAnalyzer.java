package lexemeanalizer;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import lexemeanalizer.model.ObjLexicalAnalizer;
import lexemeanalizer.model.Token;

public class LexemeAnalyzer {

    public static final String ANSI_RED = "\u001B[31m";
    public static final String ANSI_RESET = "\u001B[0m";

//    private static String TOKEN = "\nR2 := T3 semiminus T4\n";
//    private static String TOKEN = "\nR3 := T5 semiminus T6 intersec T7\n";
//    private static String TOKEN = "\nT12 := TE4 ITERSECT (EML union DEP2 WHERE SALARY > 4015); \n".trim();
//    private static String TOKEN = "\nBeginRA Create table CYCLE (KodCycl integer, NazvCycl text (60)); Insert into CYCLE values (1, 'Цикл ГСЕ дисциплін вибору') EndRA\n\n";
//    R1 := T1 where a1 = "winter" & a2 <= 30 ; 
//    R2 := T3 semiminus T4; 
//    R3 := T5 left join T6  on a3 semiminus T7 ; 
//    private static String TOKEN = "\nR4 := T8 left join (T9  semiminus T10) on a2b";


    private static String TOKEN = "";
    
    private static List<String> keywordList = new ArrayList<>(Arrays.asList(
            "itersect", "intersec", "union", "where", "semiminus", "beginra", "endra", 
            "create", "table", "integer", "text", "insert", "into", "values", "on", "left join", 
            "devide by"));
    private static List<String> relopList = new ArrayList<>(Arrays.asList(">", "<", "<=", "&"));
    private static Map<String, Integer> lexixalMap = new HashMap<>();

    public static List<ObjLexicalAnalizer> analysis(String token) {
        TOKEN = token;
        LexemeAnalyzer main = new LexemeAnalyzer();

        System.out.println(ANSI_RED + token.trim() + ANSI_RESET);
        
        System.out.println("Токен \t\t\t Лексема \t\t\t Початок \t\t Довжина \t\t Позиція");
        return main.showInfo();
    }

    private List<ObjLexicalAnalizer> showInfo() {
        ArrayList<String> items =  new ArrayList<>(Arrays.asList(TOKEN.replaceAll("\\s+", " ").replaceAll("\\(|\\)|\\;|\\,|\\n|\\t", "").trim().split(" ")));
        
        items.remove(":=");
        items = searchLiteral(items);
        findBinaryWord(items);
        
        System.out.println(items);
        
        TOKEN = TOKEN.replaceAll("\\s+", "");
        System.out.println(TOKEN);
        
        List<ObjLexicalAnalizer> list = new ArrayList<>();
        
        int positionInArray = 0;
        
        for(int i = 0; i < items.size(); i++) {
            
            int startInToken = getStartFromToken(items.get(i)) + 1;

            if(!lexixalMap.isEmpty()) {
                boolean isLexixalExists = lexixalMap.containsKey(items.get(i));
                if(isLexixalExists) {
                    int position = lexixalMap.get(items.get(i));
                    list.add(saveInfo(items, i, startInToken, position+1));
                } else {
                    positionInArray++;
                    list.add(saveInfo(items, i, startInToken, positionInArray));
                    lexixalMap.put(items.get(i), i);
                }
                
            } else {
                positionInArray++;
                list.add(saveInfo(items, i, startInToken, positionInArray));
                lexixalMap.put(items.get(i), i);
            }
            
        }
        return list;
    }
    
    private static void findBinaryWord(ArrayList<String> items) {
//        System.out.println(items);

        for(int i = 0; i < items.size()-1; i++) {
            int j = i;

            String word = items.get(j) + " " + items.get(++j);
//            System.out.println(word);
            if(keywordList.contains(word)) {
                items.set(i, word);
                items.remove(j);
            }

        }
//        System.out.println(items);
    }
    
    private static ObjLexicalAnalizer saveInfo(ArrayList<String> items, int position, int startInToken, int positionInArray) {
        if(keywordList.contains(items.get(position).toLowerCase())) {            
            System.out.println("keyword \t\t " + items.get(position)+ " \t\t\t " + startInToken + " \t\t\t " + items.get(position).length() + " \t\t\t " + positionInArray  );
            return setValue(Token.KEYWORD, items.get(position), startInToken, items.get(position).length(), positionInArray);
        } else if(relopList.contains(items.get(position).toLowerCase())) {            
            System.out.println("relop \t\t\t " + items.get(position)+ " \t\t\t\t\t " + startInToken + " \t\t\t\t " + items.get(position).length() + " \t\t\t " + positionInArray );
            return setValue(Token.RELOP, items.get(position), startInToken, items.get(position).length(), positionInArray);
        } else {
            if(Pattern.matches("[a-zA-Z]+\\d*[a-zA-Z0-9]*", items.get(position))) {                
                System.out.println("id \t\t\t " + items.get(position)+ " \t\t\t\t " + startInToken + " \t\t\t " + items.get(position).length() + " \t\t\t "  + positionInArray);
                return setValue(Token.ID, items.get(position), startInToken, items.get(position).length(), positionInArray);
            } else if (Pattern.matches("\\d+", items.get(position))) {                
                System.out.println("num \t\t\t " + items.get(position)+ " \t\t\t\t " + startInToken + " \t\t " + items.get(position).length() + " \t\t\t " + positionInArray );
                return setValue(Token.NUM, items.get(position), startInToken, items.get(position).length(), positionInArray);
            } else if (items.get(position).startsWith("\'")) {
                System.out.println("literal \t\t " + items.get(position)+ " \t " + startInToken + " \t\t " + items.get(position).length() + " \t\t\t " + positionInArray );
                return setValue(Token.LITERAL, items.get(position), startInToken, items.get(position).length(), positionInArray);
                
            }
        }
        return null;
    }
    
    private static ObjLexicalAnalizer setValue(Token token, String lexeme, int start, int length, int position) {
        ObjLexicalAnalizer lexicalAnalizer = new ObjLexicalAnalizer();
        lexicalAnalizer.setToken(token);
        lexicalAnalizer.setLexeme(lexeme);
        lexicalAnalizer.setStart(start);
        lexicalAnalizer.setLength(length);
        lexicalAnalizer.setPosition(position);
        return lexicalAnalizer;
    }
    
    private ArrayList<String> searchLiteral(ArrayList<String> items) {
        ArrayList<String> newToken = new ArrayList<>();

        for(int i = 0; i < items.size(); i++) {

            if(items.get(i).startsWith("\'")) {
                boolean first = true;
                for(int j = i; j < items.size(); j++) {
                    while (!items.get(j).endsWith("\'")) {
                        if(first) {
                            newToken.add((newToken.size()), items.get(j));
                        } else {
                            newToken.add((newToken.size()), newToken.get(newToken.size()-1) + " " + items.get(j));
                            newToken.remove(newToken.get(newToken.size()-2));
                        }
                        first = false;
                        break;
                    }
                    if(items.get(j).endsWith("\'")) {
                        if(first) {
                            newToken.add((newToken.size()), items.get(j));
                        } else {
                            newToken.add((newToken.size()), newToken.get(newToken.size()-1) + " " + items.get(j));
                            newToken.remove(newToken.get(newToken.size()-2));
                        }
                        break;
                    }
                    i = j+1;
                }
            } else {
                newToken.add(items.get(i));
            }
            
        }
//        System.out.println();
//        System.out.println(items);
//        System.out.println(newToken);

        return newToken;
    }

    private static int getStartFromToken(String item){
        Integer start = null;
        try {
            Pattern word = Pattern.compile(item.replaceAll(" ", ""));
            Matcher match = word.matcher(TOKEN);

            while (match.find()) {
                start = match.start();
            }
            return start;
        } catch (NullPointerException e) {
            System.out.println("eee");
        }

        return 0;
    }
    
    public static Map<String, Boolean> checkToken(String token) {
        Map<String, Boolean> returnMap = new HashMap<String, Boolean>();
        List<String> items = new ArrayList<>();

        if(token.contains("'")) { items.add("'"); }
        if(token.contains("(")) { items.add("("); }
        if(token.contains(")")) { items.add(")"); }

        Map<String, Integer> countItems = new HashMap<>();
        for(int i = 0; i < items.size(); i++) {
            int count = 0;
            for(int j = 0; j < token.length(); j++) {
                if(items.get(i).equals(String.valueOf(token.charAt(j))  ) ) {
                    count++;
                }
            }
            countItems.put(items.get(i), count);
        }
        
        boolean escaping = true;
        boolean openBracket = true;
        boolean closeBracket = true;
        
        for (Map.Entry<String, Integer> entry : countItems.entrySet()) {
            String key = entry.getKey();
            Integer value = entry.getValue();
            
            switch(key) {
                case "'":
                    if(value % 2 != 0) {
                        escaping = false;
                    } else {
                        escaping = true;
                    }
                    break;
                    
                case "(":
                    for (Map.Entry<String, Integer> item : countItems.entrySet()) {
                        String itemKey = item.getKey();
                        Integer itemValue = item.getValue();
                        
                        if(token.contains(")")) {
                            switch(itemKey) {
                                case ")":
                                    if(value != itemValue) {
                                        openBracket = false;
                                    } else {
                                        openBracket = true;
                                    }
                            }
                        } else {
                            openBracket = false;
                        }

                    }
                    break;
                case ")":
                    for (Map.Entry<String, Integer> item : countItems.entrySet()) {
                        String itemKey = item.getKey();
                        Integer itemValue = item.getValue();
                        
                        if(token.contains("(")) {
                            switch(itemKey) {
                                case "(":
                                    if(value != itemValue) {
                                        openBracket = false;
                                    } else {
                                        openBracket = true;
                                    }
                            }
                        } else {
                            openBracket = false;
                        }
                        
                    }
                    break;
                    
            }
            
        }
        
        if(!escaping) { returnMap.put(" ' not odd!!!", false); }
        if(!openBracket) { returnMap.put(" ( not not equal with )", false); }
        if(!closeBracket) { returnMap.put(" ) not not equal with (", false); }
        
        return returnMap;
    }
    
    
}

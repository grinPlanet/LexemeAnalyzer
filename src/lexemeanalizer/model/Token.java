package lexemeanalizer.model;

public enum Token {
    ID("id"), RELOP("relop"), NUM("num"), KEYWORD("keyword"), LITERAL("literal");
    
//    ITERSECT("ITERSECT"),
//    UNION("UNION"),
//    WHERE("WHERE"),
//    SEMIMINUS("SEMIMINUS"),
//    MORE(">"),
//    LESS("<");

    private String keyword;

    Token(String keyword) { this.keyword = keyword; }

    public String getKeyword() { return keyword; }

}
